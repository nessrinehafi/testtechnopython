# testTechnicolor.py
# Tasks: Scrapping From amazon list of E-books
# Nessrine Hafi


# Start 
# Import Needed Modules
import requests 
from bs4 import BeautifulSoup
from googlesearch import search
import ssl
import PyPDF2 
import io
import urllib.request as urllib2

# Setting Requests to be capable of to use GET request 
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'TE': 'Trailers'
}

ssl._create_default_https_context = ssl._create_unverified_context
linkFreePDF = ['http://letters.to.stephanie.gportal.hu/portal/letters.to.stephanie/upload/745843_1406744742_07068.pdf','https://www.gutenberg.org/files/1342/old/pandp12p.pdf']
titlesFreeName = ['Bell Jar' , 'Pride and Prejudice']
# Get number of page of a given Link to a PDF 
def getPages(link):
 response = requests.get(link)
 pdf_file = io.BytesIO(response.content)
 pdf_reader = PyPDF2.PdfFileReader(pdf_file)
 num_pages = pdf_reader.numPages
 print(num_pages)
ans=True
# Getting Paid Books Data

titles = []
# Issuing a GET Request to get website HTML 
amazonHTML = requests.get('https://www.amazon.com/Best-Sellers-Kindle-Store-History/zgbs/digital-text/156576011/ref=zg_bs_fvp_f_p_156576011', headers=headers)
# Parsing amazonHTML using BeautifulSoup 
soup = BeautifulSoup(amazonHTML.content, 'html.parser')
# Getting Data inside <a> with a-link-normal Class
Links = soup.find_all('a', attrs={'class': 'a-link-normal'})
# Getting Book titles inside <div> and storing them inside a List 
for l in Links:
 titl = l.find("div" ,attrs={"class": "p13n-sc-truncate"}) 
 if titl is not None:
   titles.append(titl.text.strip())
   
   
# Getting Free Books Data

titlesFree = []
# Issuing a GET Request to get website HTML 
amazonFHTML = requests.get('https://www.amazon.com/Best-Sellers-Kindle-Store-History/zgbs/digital-text/156576011/ref=zg_bs?_encoding=UTF8&tf=1', headers=headers)
# Parsing amazonFHTML using BeautifulSoup 
soupFree = BeautifulSoup(amazonFHTML.content, 'html.parser')
# Getting Data inside <a> with a-link-normal Class
linksFree = soupFree.find_all('a', attrs={'class': 'a-link-normal'})
# Getting Book titles inside <div> and storing them inside titleFree List 
for l in linksFree:
 titleFree = l.find("div" ,attrs={"class": "p13n-sc-truncate"}) 
 if titleFree is not None:
   titlesFree.append(titleFree.text.strip())

# Creating a Menu To divide task

while ans:
# Print available Choices
    print ("""
    1. List all Amazon paid history kindle ebooks
    2. Search Amazon Paid Books
    3. Search Amazon Free Books
    4. List of free E-books available on Google as PDF
    5. Download free E-books
    6.Exit/Quit
    """)
# defining choice input 
    ans=input("What would you like to do? ") 
    
    if ans=="1": 

# Printing titles List using a For Loop
      for t in titles:
       print("-"+t)
    elif ans=="2":
# defining the Book to look for input 
     b = input('Enter your Paid Book Name:')
# Printing title if the the contains the input using a For loop 
     for t in titles:
      if t.__contains__(b) :
        print(t, "\n")
    elif ans=="3":
# defining the Book to look for input 
     b = input('Enter your Free Book Name:')
# Printing title if the the contains the input using a For loop 
     for t in titlesFree:
      if t.__contains__(b) :
        print(t, "\n")
    elif ans=="4":
# Loop for each free book from titlesFree List
     for t in titlesFree:
# Define a variable that contains the name of the book with filetype pdf
      linkGoogle = '"'+t+'"'+" filetype:pdf"
# Loop for Each 10 Search of the 10 first links
      for url in search(linkGoogle, stop=5):
# Get type of Link 
       #content_type = url.handlers.get('content-type' )
       f = urllib2.urlopen(url)
# Filter wether our link is a PDF
       if 'application/pdf' in f.headers['Content-Type']:
# Getting Number of pages of the Pdf Url
        pn = getPages(url)
        p = int(0 if pn is None else pn)

# Filter PDF who has more than 50 pages
        if(p > 50):
# Display the name of available PDF and Them to a List
           print(t)
           linkFreePDF.append(url)
           titlesFreeName.append(t)
    elif ans=="5":
# Loop for Each Link for free Book
     for i in range(len(titlesFree)):
# Download Book With a given URL and title
      response = requests.get(linkFreePDF[i])
      file = open(titlesFreeName[i]+".pdf", "wb")
      file.write(response.content)
      file.close()
     print('Book Downloaded') 
    elif ans=="6":
     break
    elif ans !="":
      print("\n Not Valid Choice Try again") 



# End
